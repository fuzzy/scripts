#!/bin/bash
time=$(date +%H)

#add 7 to time for timezone conversion (cst>bmt, adjust for your timezone)
#time=$((10#$time+7))
time=$((10#$time+6))

#if the time is greater than or equal to 24, subtract 24 to 'roll it over' ie. 28 would become 4
if [[ $time -ge 24 ]]; then
	time=$((time-24))
fi

#convert hours to minutes
time=$((time*60))
#add minutes from current time
time=$(echo $time+`date +%M` | bc -l)
#convert minutes to seconds
time=$((time*60))
#add seconds from current time
time=$(echo $time+`date +%S` | bc -l)

#86.4 seconds = 1 beat
conversionRate="86.4"

#do the math to figure out the swatch internet time
swatch=$(echo $time/$conversionRate | bc -l)

#if our time is less than 100 we want to insert a 0 before it and keep just the first two characters

if [[ $(echo $swatch | cut -c2) = '.' ]];then
	echo '@00'${swatch:0:1} 
elif [[ $(echo $swatch | cut -c3) = '.' ]];then
	echo '@0'${swatch:0:2} #bash substring
else #otherwise simply add the '@' and keep just the first 3 characters
	echo '@'${swatch:0:3}
fi
