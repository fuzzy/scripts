#!/bin/sh

cache_dir=$(clipctl cache-dir)
cache_file=$cache_dir/line_cache

list_clips() {
        LC_ALL=C sort -rnk 1 < "$cache_file" | cut -d' ' -f2- | awk '!seen[$0]++'
}


list_clips | xmenu -i | tr -d '\n' | xclip -in -selection clipboard

