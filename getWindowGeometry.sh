#!/bin/fish

xwininfo -shape -id (xdotool getwindowfocus) | grep ge | cut -d " " -f 4 | cut -d "+" -f 1 | cut -d "-" -f 1
