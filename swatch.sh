#!/bin/bash
hour=$(date +%H)
minute=$(date +%M)
second=$(date +%S)

hour=$(echo $hour+6 | bc -l)
if [[ $hour -ge 24 ]]; then
	hour=$((hour-24))
fi

hour=$(echo $hour*60 | bc -l)
minute=$(echo $hour+$minute | bc -l)
minute=$(echo $minute*60 | bc -l)
second=$(echo "$minute+$second" | bc -l)

beats=$(echo $second/86.4 | bc -l)

beats=$(echo $beats | cut -d '.' -f1)

beats=$(printf "%03d" $beats)
echo "@"$beats
