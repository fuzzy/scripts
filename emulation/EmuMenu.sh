#!/bin/bash

case $1 in

	'Nintendo Entertainment System')
		ls -1 /mnt/Games/ROMS/NES
		;;
	'Super Nintendo Entertainment System')
		ls -1 /mnt/Games/ROMS/SNES
		;;
	'Nintedo 64')
		ls -1 /mnt/Games/ROMS/N64
		;;
	'Nintendo GameCube')
		ls -1 /mnt/Games/ROMS/GCN
		;;
	'Nintendo Wii')
		ls -1 /mnt/Games/ROMS/Wii
		;;
	'Nintendo GameBoy')
		ls -1 /mnt/Games/ROMS/GB && ls -1 /mnt/Games/ROMS/GBC
		;;
	'Nintendo GameBoy Advance')
		ls -1 /mnt/Games/ROMS/GBA
		;;
	'Nintendo DS')
		ls -1 /mnt/Games/ROMS/NDS
		;;
	'Sega Master System')
		ls -1 /mnt/Games/ROMS/SMS
		;;
	'Sega Genesis')
		ls -1 /mnt/Games/ROMS/SMD
		;;
	'Sega CD')
		ls -1 /mnt/Games/ROMS/SMCD
		;;
	'Sega 32X')
		ls -1 /mnt/Games/ROMS/32X
		;;
	'Sega Saturn')
		ls -1 /mnt/Games/ROMS/SS
		;;
	'Sega Dreamcast')
		ls -1 /mnt/Games/ROMS/DC
		;;
	'Sega GameGear')
		ls -1 /mnt/Games/ROMS/SGG
		;;
	'NEC PC Engine')
		ls -1 /mnt/Games/ROMS/PCE
		;;
	'NEC PC-FX')
		ls -1 /mnt/Games/ROMS/PCFX
		;;
	'Sony PlayStation')
		ls -1 /mnt/Games/ROMS/PSX
		;;
	'Sony PlayStation 2')
		ls -1 /mnt/Games/ROMS/PS2
		;;
	'Sony PlayStation Portable')
		ls -1 /mnt/Games/ROMS/PSP
		;;
	'Microsoft XBox 360')
		ls -1 /mnt/Games/ROMS/360
		;;
	*)
		echo "shits broke"
		;;
esac

