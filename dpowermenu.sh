#!/bin/sh
SELECT=$(echo -e "Log Out\nReboot\nHibernate\nShut Down" | dmenu -i -l 4 -p "Select:")
case $SELECT in
	"Log Out")sudo killall dwm;;
	"Reboot")sudo reboot;;
	"Hibernate")sleep;;
	"Shut Down")sudo poweroff;;
esac
