#!/bin/bash
textFileName=$1
inputName=$2

while read line; do
        time=0
        outputName=""
        duration=0
        i=0
        for word in $line; do
                i=$((i+1))
                
                if [[ "$i" == "1" ]]; then
                        time=$word
                elif [[ "$i" == "2" ]]; then
                        outputName=$word
                elif [[ "$i" == "3" ]]; then
                        duration=$word
                        ffmpeg -i ${inputName} -ss ${time} -t ${duration} ${outputName}.mkv &
                        $wait #!                        
                        i=0
                fi
        done 

        #echo "line"
done<$textFileName

