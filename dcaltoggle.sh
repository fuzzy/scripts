#!/bin/bash

if [[ $( pidof 'dcal' ) ]]; then
	killall dcal
else
	dcal -k -x -1190 -y 38 -of "#8c8c7f" -hf"#8f6f8f" -tf"#ff9800"
fi
