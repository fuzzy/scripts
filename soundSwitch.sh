#!/bin/bash
currentDevice=$( pactl list sinks | grep "Active Port: a" )

if [[ $currentDevice = "	Active Port: analog-output-lineout" ]]; then

	( pactl set-sink-port 1 analog-output-headphones )
	echo "switched to headphones!"
	notify-send 'Sound Switch' 'Audio device switched to headphones!'
else

	( pactl set-sink-port 1 analog-output-lineout )
	echo "switched to speakers!"
	notify-send 'Sound Switch' 'Audio device switched to speakers!'
fi
