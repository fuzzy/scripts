#!/bin/bash

x=$(xdotool getmouselocation | cut -f 1 -d " " | cut -f 2 -d ":")

if [[ x -le 2560 ]]; then
	maim /home/fuzzy/Pictures/screenshots/$(date +%s).png -g 2560x1440+0+0
else
	maim /home/fuzzy/Pictures/screenshots/$(date +%s).png -g 2560x1440+0+2561
fi
