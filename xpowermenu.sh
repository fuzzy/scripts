#!/bin/sh
cat <<EOF | xmenu -i | sh &
  Log Out	sudo kill dwm
  Reboot	sudo reboot
  Hibernate	sudo zzz
  Shut Down	sudo poweroff
EOF
